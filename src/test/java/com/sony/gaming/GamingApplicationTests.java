package com.sony.gaming;

import com.sony.gaming.controller.GameController;
import com.sony.gaming.service.GameService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class GamingApplicationTests {

    @Autowired
    private GameController controller;

    @Autowired
    private GameService service;

    @Test
    public void contextLoads() {
        assertThat(controller).isNotNull();
        assertThat(service).isNotNull();
    }

}
