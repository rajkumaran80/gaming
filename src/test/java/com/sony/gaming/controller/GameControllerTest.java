package com.sony.gaming.controller;

import com.sony.gaming.controller.error.ErrorMessage;
import com.sony.gaming.domain.Game;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;

import java.util.Objects;

import static java.util.Objects.requireNonNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class GameControllerTest {

    @Value(value = "${local.server.port}")
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @BeforeEach
    public void setup() {
        restTemplate.getRestTemplate().setRequestFactory(new HttpComponentsClientHttpRequestFactory());
    }

    @Test
    public void test_CreateGame_Successfully() {
        final String name = RandomStringUtils.randomAlphabetic(10);

        final ResponseEntity<ErrorMessage> errorResponse = this.restTemplate.getForEntity("http://localhost:" + port + "/game/" + name, ErrorMessage.class);
        assertEquals(HttpStatus.NOT_FOUND, errorResponse.getStatusCode());

        final ResponseEntity<Game> gameResponse = this.restTemplate.postForEntity("http://localhost:" + port + "/game", new Game().setName(name), Game.class);
        assertEquals(HttpStatus.CREATED, gameResponse.getStatusCode());
        assertNotNull(gameResponse.getBody());

        final ResponseEntity<Game[]> gamesResponse = this.restTemplate.getForEntity("http://localhost:" + port + "/game", Game[].class);
        assertEquals(HttpStatus.OK, gamesResponse.getStatusCode());
        assertTrue(gamesResponse.getBody() != null && gamesResponse.getBody().length > 0);
    }

    @Test
    public void test_CreatingDuplicateGames_Fails() {
        final String name = RandomStringUtils.randomAlphabetic(10);

        final ResponseEntity<Game> gameResponse = this.restTemplate.postForEntity("http://localhost:" + port + "/game", new Game().setName(name), Game.class);
        assertEquals(HttpStatus.CREATED, gameResponse.getStatusCode());

        final ResponseEntity<ErrorMessage> errorResponse = this.restTemplate.postForEntity("http://localhost:" + port + "/game", new Game().setName(name), ErrorMessage.class);
        assertEquals(HttpStatus.BAD_REQUEST, errorResponse.getStatusCode());
    }

    @Test
    public void test_StartGame_Successfully() {
        final String name = RandomStringUtils.randomAlphabetic(10);

        final ResponseEntity<Game> gameResponse = this.restTemplate.postForEntity("http://localhost:" + port + "/game", new Game().setName(name), Game.class);
        assertEquals(HttpStatus.CREATED, gameResponse.getStatusCode());
        assertFalse(requireNonNull(gameResponse.getBody()).isActive());

        final ResponseEntity<Game> gameStartedResponse = this.restTemplate.exchange("http://localhost:" + port + "/game/" + name + "/start", HttpMethod.PATCH, null, Game.class);
        assertEquals(HttpStatus.OK, gameStartedResponse.getStatusCode());
        assertTrue(requireNonNull(gameStartedResponse.getBody()).isActive());
    }

    @Test
    public void test_StartAlreadyStartedGame_Fails() {
        final String name = RandomStringUtils.randomAlphabetic(10);

        final ResponseEntity<Game> gameResponse = this.restTemplate.postForEntity("http://localhost:" + port + "/game", new Game().setName(name), Game.class);
        assertEquals(HttpStatus.CREATED, gameResponse.getStatusCode());

        final ResponseEntity<Game> gameStartedResponse = this.restTemplate.exchange("http://localhost:" + port + "/game/" + name + "/start", HttpMethod.PATCH, null, Game.class);
        assertEquals(HttpStatus.OK, gameStartedResponse.getStatusCode());
        assertTrue(requireNonNull(gameStartedResponse.getBody()).isActive());

        final ResponseEntity<ErrorMessage> errorResponse = this.restTemplate.exchange("http://localhost:" + port + "/game/" + name + "/start", HttpMethod.PATCH, null, ErrorMessage.class);
        assertEquals(HttpStatus.BAD_REQUEST, errorResponse.getStatusCode());
    }

    @Test
    public void test_StopGame_Successfully() {
        final String name = RandomStringUtils.randomAlphabetic(10);

        final ResponseEntity<Game> gameResponse = this.restTemplate.postForEntity("http://localhost:" + port + "/game", new Game().setName(name), Game.class);
        assertEquals(HttpStatus.CREATED, gameResponse.getStatusCode());

        final ResponseEntity<Game> gameStartedResponse = this.restTemplate.exchange("http://localhost:" + port + "/game/" + name + "/start", HttpMethod.PATCH, null, Game.class);
        assertEquals(HttpStatus.OK, gameStartedResponse.getStatusCode());
        assertTrue(requireNonNull(gameStartedResponse.getBody()).isActive());

        final ResponseEntity<Game> gameStopResponse = this.restTemplate.exchange("http://localhost:" + port + "/game/" + name + "/stop", HttpMethod.PATCH, null, Game.class);
        assertEquals(HttpStatus.OK, gameStopResponse.getStatusCode());
        assertFalse(requireNonNull(gameStopResponse.getBody()).isActive());
    }

    @Test
    public void test_StopAlreadyStoppedGame_Fails() {
        final String name = RandomStringUtils.randomAlphabetic(10);

        final ResponseEntity<Game> gameResponse = this.restTemplate.postForEntity("http://localhost:" + port + "/game", new Game().setName(name), Game.class);
        assertEquals(HttpStatus.CREATED, gameResponse.getStatusCode());

        final ResponseEntity<Game> gameStartedResponse = this.restTemplate.exchange("http://localhost:" + port + "/game/" + name + "/start", HttpMethod.PATCH, null, Game.class);
        assertEquals(HttpStatus.OK, gameStartedResponse.getStatusCode());
        assertTrue(requireNonNull(gameStartedResponse.getBody()).isActive());

        final ResponseEntity<Game> gameStopResponse = this.restTemplate.exchange("http://localhost:" + port + "/game/" + name + "/stop", HttpMethod.PATCH, null, Game.class);
        assertEquals(HttpStatus.OK, gameStopResponse.getStatusCode());
        assertFalse(requireNonNull(gameStopResponse.getBody()).isActive());

        final ResponseEntity<ErrorMessage> errorResponse = this.restTemplate.exchange("http://localhost:" + port + "/game/" + name + "/stop", HttpMethod.PATCH, null, ErrorMessage.class);
        assertEquals(HttpStatus.BAD_REQUEST, errorResponse.getStatusCode());
    }

    @Test
    public void test_DeleteGame_Successfully() {
        final String name = RandomStringUtils.randomAlphabetic(10);

        final ResponseEntity<Game> gameResponse = this.restTemplate.postForEntity("http://localhost:" + port + "/game", new Game().setName(name), Game.class);
        assertEquals(HttpStatus.CREATED, gameResponse.getStatusCode());

        final ResponseEntity<Void> deleteResponse = this.restTemplate.exchange("http://localhost:" + port + "/game/" + name, HttpMethod.DELETE, null, Void.class);
        assertEquals(HttpStatus.NO_CONTENT, deleteResponse.getStatusCode());

        final ResponseEntity<ErrorMessage> errorResponse = this.restTemplate.getForEntity("http://localhost:" + port + "/game/" + name, ErrorMessage.class);
        assertEquals(HttpStatus.NOT_FOUND, errorResponse.getStatusCode());
    }

}
