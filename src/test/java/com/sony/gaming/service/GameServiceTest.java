package com.sony.gaming.service;

import com.sony.gaming.domain.Game;
import com.sony.gaming.exception.GameAlreadyExistsException;
import com.sony.gaming.exception.GameInWrongStateException;
import com.sony.gaming.exception.GameNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class GameServiceTest {
    public static final String POKER = "Poker";
    public static final String CRICKET = "Cricket";
    private GameService gameService;

    @BeforeEach
    public void init() {
        gameService = new GameServiceImpl();
    }

    @Test
    public void test_GetNonExistingGame_Fails() {
        assertThrows(GameNotFoundException.class, () -> gameService.getGame(POKER));
    }

    @Test
    public void test_GetExistingGame_Successfully() throws GameAlreadyExistsException, GameNotFoundException {
        final Game gameCreated = gameService.createGame(new Game().setName(POKER));
        assertNotNull(gameCreated);

        final Game game = gameService.getGame(POKER);
        assertNotNull(game);
        assertEquals(POKER, game.getName());
        assertNotNull(game.getDateOfCreation());
        assertFalse(game.isActive());
    }

    @Test
    public void test_GetAllGames_Successfully() throws GameAlreadyExistsException {
        final Game gamePoker = gameService.createGame(new Game().setName(POKER));
        assertNotNull(gamePoker);
        final Game gameCricket = gameService.createGame(new Game().setName(CRICKET));
        assertNotNull(gameCricket);

        final List<Game> games = gameService.getAllGames();
        assertNotNull(games);
        assertThat(games).hasSameElementsAs(List.of(gamePoker, gameCricket));
    }

    @Test
    public void test_CreateGame_Successfully() throws GameAlreadyExistsException {
        final Game game = gameService.createGame(new Game().setName(POKER));
        assertNotNull(game);
        assertEquals(POKER, game.getName());
        assertNotNull(game.getDateOfCreation());
        assertFalse(game.isActive());
    }

    @Test
    public void test_CreateDuplicateGame_Fails() throws GameAlreadyExistsException {
        final Game game = gameService.createGame(new Game().setName(POKER));
        assertNotNull(game);
        assertThrows(GameAlreadyExistsException.class, () -> gameService.createGame(new Game().setName(POKER)));
    }

    @Test
    public void test_CreateDuplicateGamesSimultaneously_Fails() {
        final int[] errorCount = {0};
        IntStream.range(0, 10).parallel().forEach(i -> {
            try {
                gameService.createGame(new Game().setName(POKER));
            } catch (GameAlreadyExistsException e) {
                errorCount[0]++;
            }
        });
        assertEquals(9, errorCount[0]);
    }

    @Test
    public void test_StartGame_SetActiveToTrue() throws GameAlreadyExistsException, GameNotFoundException, GameInWrongStateException {
        Game game = gameService.createGame(new Game().setName(POKER));
        assertNotNull(game);
        assertFalse(game.isActive());

        game = gameService.startGame(POKER);
        assertNotNull(game);
        assertTrue(game.isActive());

        game = gameService.getGame(POKER);
        assertNotNull(game);
        assertTrue(game.isActive());
    }

    @Test
    public void test_StartANonExistingGame_Fails() {
        assertThrows(GameNotFoundException.class, () -> gameService.startGame(POKER));
    }

    @Test
    public void test_StopGame_SetActiveToFalse() throws GameAlreadyExistsException, GameNotFoundException, GameInWrongStateException {
        Game game = gameService.createGame(new Game().setName(POKER));
        assertNotNull(game);
        assertFalse(game.isActive());

        game = gameService.startGame(POKER);
        assertNotNull(game);
        assertTrue(game.isActive());

        game = gameService.stopGame(POKER);
        assertNotNull(game);
        assertFalse(game.isActive());

        game = gameService.getGame(POKER);
        assertNotNull(game);
        assertFalse(game.isActive());
    }

    @Test
    public void test_StoppingANonExistingGame_Fails() {
        assertThrows(GameNotFoundException.class, () -> gameService.stopGame(POKER));
    }

    @Test
    public void test_DeleteGame_Successfully() throws GameAlreadyExistsException, GameNotFoundException {
        final Game gameCreated = gameService.createGame(new Game().setName(POKER));
        assertNotNull(gameCreated);

        gameService.deleteGame(POKER);

        assertThrows(GameNotFoundException.class, () -> gameService.getGame(POKER));
    }

    @Test
    public void test_DeleteANonExistingGame_Fails() {
        assertThrows(GameNotFoundException.class, () -> gameService.deleteGame(POKER));
    }

}
