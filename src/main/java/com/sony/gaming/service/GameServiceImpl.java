package com.sony.gaming.service;

import com.sony.gaming.domain.Game;
import com.sony.gaming.exception.GameAlreadyExistsException;
import com.sony.gaming.exception.GameInWrongStateException;
import com.sony.gaming.exception.GameNotFoundException;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class GameServiceImpl implements GameService {

    private final Map<String, Game> cache = new HashMap<>();

    @Override
    public Game getGame(final String name) throws GameNotFoundException {
        return getGameFromCache(name);
    }

    @Override
    public List<Game> getAllGames() {
        return List.copyOf(cache.values());
    }

    @Override
    public Game createGame(final Game game) throws GameAlreadyExistsException {
        final String name = game.getName();
        synchronized (name.intern()) {
            if (cache.containsKey(name)) {
                throw new GameAlreadyExistsException();
            }
            game.setDateOfCreation(LocalDateTime.now());
            cache.put(name, game);
            return game;
        }
    }

    @Override
    public void deleteGame(final String name) throws GameNotFoundException {
        synchronized (name.intern()) {
            getGameFromCache(name);
            cache.remove(name);
        }
    }

    @Override
    public Game startGame(final String name) throws GameNotFoundException, GameInWrongStateException {
        synchronized (name.intern()) {
            final Game game = getGameFromCache(name);
            if (game.isActive()) {
                throw new GameInWrongStateException("Game is already started");
            } else {
                return game.setActive(true);
            }
        }
    }

    @Override
    public Game stopGame(final String name) throws GameNotFoundException, GameInWrongStateException {
        synchronized (name.intern()) {
            final Game game = getGameFromCache(name);
            if (!game.isActive()) {
                throw new GameInWrongStateException("Game is not started yet");
            } else {
                return game.setActive(false);
            }
        }
    }

    private Game getGameFromCache(final String name) throws GameNotFoundException {
        final Game game = cache.get(name);
        if (game == null) {
            throw new GameNotFoundException();
        }
        return game;
    }
}
