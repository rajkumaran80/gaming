package com.sony.gaming.service;

import com.sony.gaming.domain.Game;
import com.sony.gaming.exception.GameAlreadyExistsException;
import com.sony.gaming.exception.GameInWrongStateException;
import com.sony.gaming.exception.GameNotFoundException;

import java.util.List;

public interface GameService {
    Game getGame(final String name) throws GameNotFoundException;

    List<Game> getAllGames();

    Game createGame(final Game game) throws GameAlreadyExistsException;

    void deleteGame(final String name) throws GameNotFoundException;

    Game startGame(final String name) throws GameNotFoundException, GameInWrongStateException;

    Game stopGame(final String name) throws GameNotFoundException, GameInWrongStateException;
}
