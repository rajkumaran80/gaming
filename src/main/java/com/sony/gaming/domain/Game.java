package com.sony.gaming.domain;

import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@Data
@Accessors(chain = true)
public class Game {
    private String name;
    private LocalDateTime dateOfCreation;
    private boolean active;
}
