package com.sony.gaming.exception;

public class GameInWrongStateException extends Exception {
    public GameInWrongStateException(final String message) {
        super(message);
    }
}
