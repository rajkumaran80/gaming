package com.sony.gaming.controller;

import com.sony.gaming.domain.Game;
import com.sony.gaming.exception.GameAlreadyExistsException;
import com.sony.gaming.exception.GameInWrongStateException;
import com.sony.gaming.exception.GameNotFoundException;
import com.sony.gaming.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/game")
public class GameController {

    private final GameService gameService;

    @Autowired
    public GameController(final GameService gameService) {
        this.gameService = gameService;
    }

    @GetMapping("/{name}")
    public ResponseEntity<Game> getGame(@PathVariable("name") final String name) throws GameNotFoundException {
        return ResponseEntity.ok(gameService.getGame(name));
    }

    @GetMapping
    public ResponseEntity<List<Game>> getAllGames() {
        return ResponseEntity.ok(gameService.getAllGames());
    }

    @PostMapping
    public ResponseEntity<Game> createGame(@RequestBody final Game game) throws GameAlreadyExistsException {
        return new ResponseEntity<>(gameService.createGame(game), HttpStatus.CREATED);
    }

    @DeleteMapping("/{name}")
    public ResponseEntity<Void> deleteGame(@PathVariable("name") final String name) throws GameNotFoundException {
        gameService.deleteGame(name);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PatchMapping("/{name}/start")
    public ResponseEntity<Game> startGame(@PathVariable("name") final String name) throws GameNotFoundException, GameInWrongStateException {
        return ResponseEntity.ok(gameService.startGame(name));
    }

    @PatchMapping("/{name}/stop")
    public ResponseEntity<Game> stopGame(@PathVariable("name") final String name) throws GameNotFoundException, GameInWrongStateException {
        return ResponseEntity.ok(gameService.stopGame(name));
    }

}
