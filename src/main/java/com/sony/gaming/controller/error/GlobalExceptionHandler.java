package com.sony.gaming.controller.error;

import com.sony.gaming.exception.GameAlreadyExistsException;
import com.sony.gaming.exception.GameInWrongStateException;
import com.sony.gaming.exception.GameNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;

import java.util.Date;

@Slf4j
@ControllerAdvice
@ResponseBody
public class GlobalExceptionHandler {

    @ExceptionHandler(GameNotFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public ErrorMessage gameNotFoundException(final GameNotFoundException e, final WebRequest request) {
        log.error("Game not found: {}", request.getContextPath(), e);
        return new ErrorMessage(404, new Date(), "Game not found", "Unable to find a game with the name provided");
    }

    @ExceptionHandler(GameAlreadyExistsException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ErrorMessage gameAlreadyExistsException(final GameAlreadyExistsException e, final WebRequest request) {
        log.error("Game already exists: {}", request.getContextPath(), e);
        return new ErrorMessage(400, new Date(), "Game already exists", "Game already exists with the name provided");
    }

    @ExceptionHandler(GameInWrongStateException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ErrorMessage gameInWrongStateException(final GameInWrongStateException e, final WebRequest request) {
        log.error("Game in wrong state: {}", request.getContextPath(), e);
        return new ErrorMessage(400, new Date(), "Game in wrong state", e.getMessage());
    }
}