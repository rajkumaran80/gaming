# gaming

## Decisions Made

ConcurrentHashMap can be used instead of HashMap as it uses multiple locks on segment level.
But I have used HashMap with my own locking to ensure the following scenarios
- A game cannot be overwritten by another game with same name
- start, stop and delete operations for a particular game is mutually exclusive
- get and list game operations are non-blocking
